
import os
import sys
sys.path.insert(0, "./facenet_inference/src")

import cv2
import Tkinter as tk
import tkMessageBox
import tkFileDialog
from PIL import Image, ImageTk
import json
import ast
import tkSimpleDialog

from PIL import Image
def check_image_with_pil(path):
    print("path: ", path)
    try:
        Image.open(path)
    except IOError:
        return False
    return True


def _from_rgb(rgb):
    """translates an rgb tuple of int to a tkinter friendly color code
    """
    return "#%02x%02x%02x" % rgb 

top = tk.Tk()
top.geometry("1150x700+100+100")

disp_width = 720
disp_height = 520
zoom_size = 150


trace = 0 

bigframe=tk.LabelFrame(top,relief=tk.GROOVE, width=disp_width+8, height=disp_height+30,bd=2, text="Image:                    ", font =("Times New Roman", 12) )
bigframe.place(x=10,y=10)

canvas = tk.Canvas(bigframe, width=disp_width, height=disp_height) # bg = _from_rgb((100,100,100))) 
canvas.place(x=0, y=4 )
_from_rgb
box_color = (0,255,0)

global id_image_crop_dict
id_image_crop_dict={}

def delete_image_in_dict(id_image_crop_dict, image):
    new_id_image_crop_dict = {}
    for id in id_image_crop_dict:
        image_crop_dict = id_image_crop_dict[id]
        if image in image_crop_dict:
            image_crop_dict.pop(image, None)
        new_id_image_crop_dict[id] = image_crop_dict
    return new_id_image_crop_dict
                  

def append_id_image_crop_dict_in_img(id_image_crop_dict, file_name, cv2image, bounding_boxes, persons):
    for i in range(len(bounding_boxes)):
        box = bounding_boxes[i]
        id = persons[i]
        crop = cv2image[box[1]:box[3], box[0]:box[2]]
        append_id_image_crop_dict(id_image_crop_dict, id, file_name, crop)
    return id_image_crop_dict

def append_id_image_crop_dict(id_image_crop_dict, id, file_name, crop):    
    if not id in id_image_crop_dict:
        image_crops = {}
        image_crops[file_name]= crop
        id_image_crop_dict[id] = image_crops
    else:
        image_crops = id_image_crop_dict[id]
        image_crops[file_name] = crop
    return id_image_crop_dict

def process_json(json_path):
    global image_name
    with open(json_path) as json_file:
        data = json.load(json_file)
        image_name = data["image_path"]
        bounding_boxes_str = data["bounding_boxes"]
        persons =  ast.literal_eval(data["person"])
        print("image_name: ", image_name)
        print("bounding_boxes_str: ", bounding_boxes_str)
        bounding_boxes = ast.literal_eval(bounding_boxes_str)
        print("bounding_boxes: ", bounding_boxes)
    return image_name, bounding_boxes, persons

def create_json_file(img_file_name, json_file_path, bounding_boxes, person_list):
    data = {}
    data["image_path"]=img_file_name
    data["bounding_boxes"]=str(bounding_boxes)
    data["person"]=str(person_list)
    with open(json_file_path, 'w') as outfile:
        json.dump(data, outfile)


def create_empty_json_file(img_file_name, json_file_path):
    data = {}
    data["image_path"]=img_file_name
    data["bounding_boxes"]=str([])
    data["person"]=str([])
    with open(json_file_path, 'w') as outfile:
        json.dump(data, outfile)

#scrollbar = tk.Scrollbar(top, orient="vertical")
#fileListbox.config(yscrollcommand=scrollbar.set)
#scrollbar.config(command=fileListbox.yview)
#scrollbar.pack(side=tk.LEFT, fill=tk.Y) #place(x=880, y=300)


##############################
zoomFrame = tk.LabelFrame(top,relief=tk.GROOVE,bd=2, width=zoom_size + 50, height=zoom_size, text="Zoom of selected box", font =("Times New Roman", 11) )
zoomFrame.place(x=760, y= 10)
zoomDisp = tk.Canvas(zoomFrame, width=zoom_size, height=zoom_size)
zoomDisp.pack(padx=15, pady=0, fill = "both")

##############################
 

class CanvasEventsDemo: 
    def __init__(self, canvas, parent=None):
#        canvas.pack()'
        canvas.bind('<ButtonPress-1>', self.onStart) 
        canvas.bind('<B1-Motion>',     self.onGrow)  
        canvas.bind('<Double-1>',      self.onAdd) 
        canvas.bind('<ButtonPress-3>', self.onMove)  
        self.canvas = canvas
        self.drawn  = None
        self.kinds = [canvas.create_rectangle]
    def onStart(self, event):
#        print("onStart")
        self.shape = self.kinds[0]
        self.kinds = self.kinds[1:] + self.kinds[:1] 
        self.start = event
        self.drawn = None
    def onGrow(self, event):                         
        canvas = event.widget
        if self.drawn: canvas.delete(self.drawn)
        objectId = self.shape(self.start.x, self.start.y, event.x, event.y, outline=_from_rgb(box_color), width= 2)
        self.box = (self.start.x, self.start.y, event.x, event.y)
        if trace: print objectId
        self.drawn = objectId
    def onAdd(self, event):
        print("onAdd")       
        global cv2image           
        width = cv2image.shape[1]
        height = cv2image.shape[0]
        x0 = int(self.box[0]*width/disp_width)
        x1 = int(self.box[2]*width/disp_width)
        y0 = int(self.box[1]*height/disp_height)
        y1 = int(self.box[3]*height/disp_height)
        box_str = str((x0,y0,x1,y1))
        new_id = tkSimpleDialog.askstring("New person id", "enter new person id", initialvalue = "-1")
        new_bbox_str = tkSimpleDialog.askstring("New bounding box", "enter new bounding box", initialvalue = box_str)
        new_text = str(new_id) + "   " + new_bbox_str
        listbox.insert(tk.END, new_text)  
        persons.append(new_id)
        new_bbox = ast.literal_eval(new_bbox_str)
        bounding_boxes.append(new_bbox)
        draw_img(cv2image, persons, bounding_boxes)
        draw_crop(new_bbox)
 
    def onMove(self, event):
        if self.drawn:                               
            if trace: print self.drawn
            canvas = event.widget
            diffX, diffY = (event.x - self.start.x), (event.y - self.start.y)
            canvas.move(self.drawn, diffX, diffY)
            self.start = event

CanvasEventsDemo(canvas)
   
#################################
def draw_listbox(bounding_boxes, persons):
   listbox.delete(0, tk.END)
   for i in range(len(persons)):
       person = persons[i]
       bounding_box = bounding_boxes[i]
       text = str(person) + "   " + str(bounding_box)
       listbox.insert(tk.END, text)

def draw_img(cv2image, persons, bounding_boxes):
   cv2image0 = cv2image.copy()  
   for i in range(len(bounding_boxes)):
       bounding_box = bounding_boxes[i]
       cv2.putText(cv2image0,str(persons[i]), (int(bounding_box[0])+5,int(bounding_box[1])+5), cv2.FONT_HERSHEY_SIMPLEX, 2, (0,0,255),2,cv2.LINE_AA)
       cv2.rectangle(cv2image0,(int(bounding_box[0]),int(bounding_box[1])),(int(bounding_box[2]),int(bounding_box[3])),box_color,2)   
   resized_cv2image = cv2.resize(cv2image0, (disp_width,disp_height))
   resized_cv2image = cv2.cvtColor(resized_cv2image, cv2.COLOR_BGR2RGB)  
   img = ImageTk.PhotoImage(image = Image.fromarray(resized_cv2image))
#   lmain.imgtk = img   
#   lmain.configure(image=img)
   canvas.img = img
   canvas.create_image(0, 0, image=img, anchor=tk.NW)

def draw_id_images(pos, id_image_crop_dict, image_name, cv2image, bounding_boxes, persons):
   append_id_image_crop_dict_in_img(id_image_crop_dict, image_name, cv2image, bounding_boxes, persons)
   personListbox.select_set(pos)
   id = id_image_crop_dict.keys()[pos]
   image_crop_dict = id_image_crop_dict[id]
   i = 0
   listcanvas.delete("all")

   for image in image_crop_dict:
       if i < (len(image_crop_dict)-10): 
           i+=1
           continue
       crop = image_crop_dict[image]    
       resized_crop = cv2.resize(crop,(c_width-5,c_width-5))    
       cv2.putText(resized_crop, image, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.30, (0, 0, 255), lineType=cv2.LINE_AA) 
       cv2.putText(resized_crop, "Person: "+str(id), (10, 10), cv2.FONT_HERSHEY_SIMPLEX, 0.30, (0, 0, 255), lineType=cv2.LINE_AA) 
       resized_crop = cv2.cvtColor(resized_crop, cv2.COLOR_BGR2RGB)  


       c = tk.Canvas(listcanvas, width=(2*c_width-10), height=(2*c_width-10))
       
       img = Image.fromarray(resized_crop)
       imgtk = ImageTk.PhotoImage(image=img)  
#       print("imgtk: ", imgtk)
       c.img = imgtk
       
       c.create_image(3*c_width/2, 1*c_width/2+10, image=imgtk)
       
       listcanvas.create_window(0, c_width*i, window=c)
#       if i==0:  
#          i+=1
#          listcanvas.create_window(0, c_width*i, window=c)
       i += 1
       listcanvas.configure(scrollregion=listcanvas.bbox("all"))

def draw_image(dir_path, json_path):
   global persons
   global bounding_boxes
   image_name, bounding_boxes, persons = process_json(json_path)
   image_path = os.path.join(dir_path,image_name)
   global cv2image
   cv2image = cv2.imread(image_path)
   draw_img(cv2image, persons, bounding_boxes)
   draw_listbox(bounding_boxes, persons)
   global id_image_crop_dict
   draw_id_images(0, id_image_crop_dict, image_name, cv2image, bounding_boxes, persons)
   
   personListbox.delete(0,tk.END)
   for id in id_image_crop_dict:
      personListbox.insert(tk.END, str(id))
   return image_name
#   print("id_image_crop_dict: ", id_image_crop_dict)
   
######################################
def openCallBack():
   global dir_path 
   dir_path = tkFileDialog.askdirectory(initialdir="/media/tran/0FF40915416FC824/working/output/collects/")
   print("dir_path: ", dir_path)
   files = os.listdir(dir_path)
   files.sort()
   global json_files 
   json_files = []
   for file in files:
       file_path = os.path.join(dir_path, file)
       ext = file.split(".")[-1]
       json_file_path = file_path.replace(ext,"json")
       if not os.path.exists(json_file_path):
           if check_image_with_pil(file_path):
                create_empty_json_file(file, json_file_path)

   for file in files:
       if file.endswith(".json"):
           
           file_path = os.path.join(dir_path,file)
           json_files.append(file_path)
           fileListbox.insert(tk.END, file.replace(".json", ""))
 #  print("json_files: ", json_files)
   global current_idx 
   current_idx = 0
   global json_path 
   json_path = json_files[0]
   image_name = draw_image(dir_path, json_path)
   global bounding_boxes
   if len(bounding_boxes)>0:    
       draw_crop(bounding_boxes[0])
#       listbox.select_set(0)
   bigframe.config(text = "Image: " + image_name)
bOpen = tk.Button(top, text ="Open Image Directory", command = openCallBack)
bOpen.place(x=10, y= 610) 

###################################
def draw_crop(box):
   croped = cv2image[box[1]:box[3], box[0]:box[2]]
   resized_cv2image = cv2.resize(croped, (zoom_size,zoom_size))
   resized_cv2image = cv2.cvtColor(resized_cv2image, cv2.COLOR_BGR2RGBA)
   img = Image.fromarray(resized_cv2image)
   imgtk = ImageTk.PhotoImage(image=img)
   zoomDisp.imgtk = imgtk   
#   zoomDisp.configure(image=imgtk)
   zoomDisp.create_image(0, 0, image=imgtk, anchor=tk.NW)
   




###########################
image_frame = tk.LabelFrame(top,relief=tk.GROOVE,bd=2, text="Browse Images", font=("Times New Roman",11))
image_frame.place(x=250,y=570)
###########################
def prevCallBack():
   global current_idx 
   current_idx -= 1
   if current_idx ==-1:
      current_idx = fileListbox.size()-1
      fileListbox.yview_scroll(fileListbox.size()-1,"units")
   else:
      fileListbox.yview_scroll(-1,"units")
   global json_path 
   global json_files
   json_path = json_files[current_idx]
   draw_image(dir_path, json_path)
   if len(bounding_boxes)>0:    
       draw_crop(bounding_boxes[0])
       listbox.select_set(0)
   fileListbox.select_set(current_idx)
   
bPrev = tk.Button(image_frame, text ="Previous Image (Ctrl+w)", command = prevCallBack)
bPrev.grid(row=0, column=0, sticky=tk.N+tk.S+tk.E+tk.W)
#bPrev.place(x=220, y= 650) 
#bPrev.pack()
def keyPrevCallBack(event):
   prevCallBack() 
top.bind('<Control-w>', keyPrevCallBack) 

###########################
def nextCallBack():
   global current_idx 
   current_idx += 1
   if current_idx ==fileListbox.size():
      current_idx = 0
      fileListbox.yview_scroll(-fileListbox.size()+1,"units")
   else:
      fileListbox.yview_scroll(1,"units")
   global json_path 
   global json_files
   
   json_path = json_files[current_idx]
   draw_image(dir_path, json_path)
   if len(bounding_boxes)>0:    
       draw_crop(bounding_boxes[0])
       listbox.select_set(0)
   fileListbox.select_set(current_idx)

bNext = tk.Button(image_frame, text ="Next Image (Ctrl+e)", command = nextCallBack)
bNext.grid(row=1, column=0, sticky=tk.N+tk.S+tk.E+tk.W)
#bNext.place(x=420, y= 650) 
#bNext.pack()
def keyNextCallBack(event):
   nextCallBack() 
top.bind('<Control-e>', keyNextCallBack)  



############################
def delImgCallBack():
   global json_path 
   global image_name
   global dir_path 
   print("dir_path: ", dir_path)
   image_path = os.path.join(dir_path, image_name)
   os.remove(json_path)
   os.remove(image_path)
   global current_idx 
   json_files.pop(current_idx)
   if current_idx == len(json_files):   
      current_idx=0
   json_path = json_files[current_idx]
   global persons
   global bounding_boxes
   image_name, bounding_boxes, persons = process_json(json_path)
   image_path = os.path.join(dir_path,image_name)
   global cv2image
   cv2image = cv2.imread(image_path)
   draw_image(dir_path, json_path)
   fileListbox.delete(0,tk.END)
   files = os.listdir(dir_path)
   files.sort()
   for file in files:
       if file.endswith(".json"):
           fileListbox.insert(tk.END, file.replace(".json", ""))

   fileListbox.select_set(current_idx)
   delete_image_in_dict(id_image_crop_dict, image_name)
   draw_id_images(0, id_image_crop_dict, image_name, cv2image, bounding_boxes, persons)

bDelImg = tk.Button(image_frame, text ="Delete Image (Ctrl+d)", command = delImgCallBack)
bDelImg.grid(row=2, column=0, sticky=tk.N+tk.S+tk.E+tk.W)
#bDelImg.place(x=600, y= 650) 
#bDelImg.pack()
def keyDelImgCallBack(event):
   delImgCallBack() 
top.bind('<Control-d>', keyDelImgCallBack)      


############################  
def fileListboxCallBack(event):
   global current_idx 
   current_idx = fileListbox.curselection()[0]
   global json_path 
   global dir_path
   json_path = json_files[current_idx]
   draw_image(dir_path, json_path)
   global bounding_boxes
   if len(bounding_boxes)>0:    
       draw_crop(bounding_boxes[0])
       listbox.select_set(0)
   return

filesb = tk.Scrollbar(image_frame, orient=tk.VERTICAL, width=15)
fileListbox = tk.Listbox(image_frame, width=30, height=8)
#vsb.pack(side=tk.RIGHT, fill=tk.Y)
filesb.grid(row=0,column=2, rowspan=3,  sticky=tk.W+tk.E+tk.N+tk.S)
filesb.config(command=fileListbox.yview)
fileListbox.config(yscrollcommand=filesb.set)

fileListbox.bind("<Double-Button-1>", fileListboxCallBack)
#fileListbox.place(x=550, y=590) 
#fileListbox.pack(side=tk.RIGHT)
fileListbox.grid(row=0, column=1, rowspan=3,sticky=tk.W+tk.E+tk.N+tk.S, padx=(10,0), pady=0)

############################
annot_frame = tk.LabelFrame(top,relief=tk.GROOVE,bd=2, text="Browse Boxes", font=("Times New Roman",11))
annot_frame.place(x=760,y=185)

############################
def addBBCallBack():
   global persons
   global bounding_boxes
   global cv2image
   person=-1
   new_id = tkSimpleDialog.askstring("New person id", "enter new person id", initialvalue = person)
   bounding_box = str((0,0,100,100))
   new_bbox_str = tkSimpleDialog.askstring("New bounding box", "enter new bounding box", initialvalue = bounding_box)
#   if not (str(new_id)==person):
   new_text = str(new_id) + "   " + new_bbox_str
   new_bbox = ast.literal_eval(new_bbox_str)
   bounding_boxes.append(new_bbox)
   persons.append(new_id)
   listbox.insert(tk.END, new_text)
   draw_img(cv2image, persons, bounding_boxes)
   draw_crop(new_bbox)
bAddBB = tk.Button(annot_frame, text ="Add Box (Ctrl+a)", command=addBBCallBack)
#bAddBB.place(x=750, y= 200)
bAddBB.grid(row=0, column=0, sticky=tk.N+tk.S+tk.E+tk.W)
def keyAddBBCallBack(event):
   addBBCallBack() 
top.bind('<Control-a>', keyAddBBCallBack)   

############################
def delBBCallBack():
   global persons
   global bounding_boxes
   global cv2image
   selected = listbox.curselection()
   selected_0 = selected[0]
   persons.pop(selected_0)
   bounding_boxes.pop(selected_0)  
   listbox.delete(selected_0)   
   draw_img(cv2image, persons, bounding_boxes)
bDelBB = tk.Button(annot_frame, text ="Del Box", command=delBBCallBack)
#bDelBB.place(x=900, y= 200) #.pack(padx=5, pady=20, side=tk.LEFT)
bDelBB.grid(row=1, column=0, sticky=tk.N+tk.S+tk.E+tk.W)

############################
def saveAnnoCallBack():
   global bounding_boxes
   global json_path 
   global image_name
   global persons
   global cv2image
   create_json_file(image_name, json_path, bounding_boxes, persons)
   draw_id_images(0, id_image_crop_dict, image_name, cv2image, bounding_boxes, persons)

bSaveAnno = tk.Button(annot_frame,  wraplength=120, justify=tk.LEFT, text ="Save Annotation (Ctrl+s)", command=saveAnnoCallBack)
#bSaveAnno.place(x=990, y= 200) 
bSaveAnno.grid(row=2, column=0, sticky=tk.N+tk.S+tk.E+tk.W)

def keySaveAnnoCallBack(event):
   saveAnnoCallBack()
top.bind('<Control-s>', keySaveAnnoCallBack)    

############################
def listboxCallBack(event):
   global persons
   global bounding_boxes
   selected = listbox.curselection()
   selected_0 = selected[0]
   text = str(listbox.get(selected_0))
   person,box_str = text.split("   ")   
   print("person: ", person)
   box = ast.literal_eval(box_str)
   print("box: ", box)  
   global cv2image
   draw_crop(box)
   new_id = tkSimpleDialog.askstring("New person id", "enter new person id", initialvalue = person)
   new_bbox_str = tkSimpleDialog.askstring("New bounding box", "enter new bounding box", initialvalue = box_str)
#   if not (str(new_id)==person):
   new_text = str(new_id) + "   " + new_bbox_str
   listbox.delete(selected_0)
   listbox.insert(selected_0, new_text)      
   persons[selected_0] = new_id 
   new_bbox = ast.literal_eval(new_bbox_str)
   bounding_boxes[selected_0] = new_bbox
   draw_img(cv2image, persons, bounding_boxes)
   draw_crop(new_bbox)   
listbox = tk.Listbox(annot_frame, width=30, height=12)
listbox.bind("<Double-Button-1>", listboxCallBack)
listbox.grid(row=0, column=1, rowspan=3,sticky=tk.W+tk.E+tk.N+tk.S, padx=10, pady=0)
############################


c_width = 156
myframe=tk.LabelFrame(top,relief=tk.GROOVE,width=c_width+4,height=2*c_width+4,bd=2, text = "Persons and their annotated images",font=("Times New Roman", 11))
myframe.place(x=760,y=360)

def personListboxCallBack(event):
   selected = personListbox.curselection()[0]
   print("personListbox.curselection: ", selected)
   global bounding_boxes
   global image_name
   global persons
   global cv2image

   draw_id_images(selected, id_image_crop_dict, image_name, cv2image, bounding_boxes, persons)
   return
personListbox = tk.Listbox(myframe, width=20, height=22)
personListbox.bind("<Double-Button-1>", personListboxCallBack)
#personListbox.place(x=780, y=390) #.pack() yscrollcommand=s.set
personListbox.grid(row=0,column=0, padx=0, sticky=tk.N+tk.S+tk.W)

vsb = tk.Scrollbar(myframe, orient=tk.VERTICAL, width=20)
#vsb.pack(side=tk.RIGHT, fill=tk.Y)
vsb.grid(row=0,column=2,  sticky=tk.N+tk.S+tk.W)

listcanvas = tk.Canvas(myframe)
vsb.config(command=listcanvas.yview)
listcanvas.config(width=c_width, height=2*c_width)
listcanvas.config(yscrollcommand=vsb.set, scrollregion=listcanvas.bbox(tk.ALL))
listcanvas.configure(scrollregion=listcanvas.bbox("all"))


#listcanvas.pack(side=tk.LEFT,expand=True,fill=tk.BOTH)
listcanvas.grid(row=0,column=1, padx=(10,0))



############################

top.mainloop()

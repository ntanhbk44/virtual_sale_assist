import sys
import json
sys.path.insert(0, "./facenet_inference/src")

import Tkinter as tk
import tkMessageBox
import tkFileDialog
import cv2
from PIL import Image, ImageTk
from ismodels import face_track_server, face_describer_server, face_db, camera_server
from configs import configs
import tensorflow as tf
import align.detect_face
from scipy import misc
import numpy as np
import os

top = tk.Tk()

#Graphics window
imageFrame = tk.Frame(top, width=800, height=500)
imageFrame.pack( padx=10, pady=2)

lmain = tk.Label(imageFrame)
lmain.grid(row=1, column=0, padx=10, pady=2)  #Display 1
#display2 = tk.Label(imageFrame)
#display2.grid(row=0, column=0) #Display 2

#back = tk.Frame(master=top, width=1000, height=600, bg='gray')
#back.pack_propagate(0) 
#back.pack()


cap = None

def create_json_file(img_file_name, json_file_path, bounding_boxes):
    data = {}
    data["image_path"]=img_file_name
    data["bounding_boxes"]=str(bounding_boxes)
    person_list = []
    for i in range(len(bounding_boxes)):
        person_list.append(-1)
    data["person"]=str(person_list)
    with open(json_file_path, 'w') as outfile:
        json.dump(data, outfile)
          

with tf.Graph().as_default():
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.3)
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
    with sess.as_default():
        pnet, rnet, onet = align.detect_face.create_mtcnn(sess, None)
    
minsize = 60 # minimum size of face
image_size = 160
margin = 16
threshold = [ 0.5, 0.6, 0.6 ]  # three steps's threshold
factor = 0.709 # scale factor

def align_face(image_path, pnet, rnet, onet, threshold, factor, minsize, image_size, margin):
    img = misc.imread(image_path)
    if img.ndim<2:
        print('Unable to align "%s"' % image_path)
        text_file.write('%s\n' % (output_filename))
        exit(2)
    if img.ndim == 2:
        img = facenet.to_rgb(img)
        img = img[:,:,0:3]

    bounding_boxes, _ = align.detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)

    nrof_faces = bounding_boxes.shape[0]
    crops = []
    scaled_paths = []
    bounding_boxes_out = []
    if nrof_faces>0:
        print("bounding_boxes: ",bounding_boxes)
        det = bounding_boxes[:,0:4]
        det_arr = []
        img_size = np.asarray(img.shape)[0:2]
        for i in range(nrof_faces):
            det_arr.append(np.squeeze(det[i]))

        for i, det in enumerate(det_arr):
            det = np.squeeze(det)
            bb = np.zeros(4, dtype=np.int32)
            bb[0] = np.maximum(det[0]-margin/2, 0)
            bb[1] = np.maximum(det[1]-margin/2, 0)
            bb[2] = np.minimum(det[2]+margin/2, img_size[1])
            bb[3] = np.minimum(det[3]+margin/2, img_size[0])
            cropped = img[bb[1]:bb[3],bb[0]:bb[2],:]
            if (abs(bb[2]-bb[0])>600) or (abs(bb[3]-bb[1])>600): continue
            crops.append(cropped)
            bounding_boxes_out.append((bb[0],bb[1],bb[2],bb[3]))

    return crops, scaled_paths, bounding_boxes_out

def openCallBack():
   file_path = tkFileDialog.askopenfilename()

   if file_path.endswith(".mkv"):
       print(file_path)
   else: return 
   cap = cv2.VideoCapture(file_path)
   count = 0
               # Check if camera opened successfully
   if (cap.isOpened()== False): print("Error opening video stream or file")

   # Default resolutions of the frame are obtained.The default resolutions are system dependent.
   # We convert the resolutions from float to integer.
   frame_width = int(cap.get(3))
   frame_height = int(cap.get(4))
   collect_dir = "../../collects/" + file_path.split("/")[-1] + "_imgs/"

   if not os.path.exists(collect_dir): os.makedirs(collect_dir)

   def show_frame(count):
      ret, frame = cap.read()
      if ret == False: return
      count += 1
      
      tmp_img_path_full = "tmp.png"
      cv2.imwrite(tmp_img_path_full, frame)
      crops, scaled_paths, bounding_boxes = align_face(tmp_img_path_full, pnet, rnet, onet, threshold, factor, minsize, image_size, margin) 
      if (len(crops)>0):
          img_file_name =  "img_" + str(count).zfill(10) + ".png"
          collect_file_path = collect_dir + img_file_name
          cv2.imwrite(collect_file_path, frame)
          json_file_path = collect_dir + "img_" + str(count).zfill(10) + ".json"
          create_json_file(img_file_name, json_file_path, bounding_boxes)

      frame = cv2.resize(frame,(600,400))

            
                     
#      frame = cv2.flip(frame, 1)
      cv2image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGBA)
      img = Image.fromarray(cv2image)
      imgtk = ImageTk.PhotoImage(image=img)
      lmain.imgtk = imgtk
      lmain.configure(image=imgtk)
      lmain.after(10, lambda : show_frame(count)) 
   show_frame(count)






bOpen = tk.Button(top, text ="OpenVideo", command = openCallBack)
bOpen.pack(padx=5, pady=10, side=tk.LEFT)



pOpen = tk.Button(top, text ="Previous")
pOpen.pack(padx=5, pady=20, side=tk.LEFT)

nOpen = tk.Button(top, text ="Next")
nOpen.pack(padx=5, pady=20, side=tk.LEFT)


top.mainloop()

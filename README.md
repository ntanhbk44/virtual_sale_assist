Components:

**1. Annotation**

a. Installation:


*  `cd Annotation`
   
 
*  Install tk tool: https://tkdocs.com/tutorial/install.html
   

*  Install python libraries: `pip install -r requirements.txt`
   

*  unzip facenet_inference.zip
   

*  `cd facenet_inference`
   

*  Install python libraries: `pip install -r requirements.txt`
   
b. Running tool to automatically extract frames containing faces in a video

  
*   `python Annotator.py`
    
  
*   press "Open Video"
   ![alt text](img/open_video.png "")
  
*   Select one video to automatically generate the corresponding foler which contains images of frames and corresponding annotation files     
    ![alt text](img/select_video.png "")

    ![alt text](img/run_video.png "")


The tool then automatically runs detection and extracts candidate frames which possibly contains faces. It also generates corresponding json annotation files.

The generated files will then be saved into corresponding folder "collects/vide_name/".

    ![alt text](img/output_frames_jsons.png "")



c. Running Annotation Tool
  
*   `python PersonAnno.py`
    
     ![alt text](img/Anno_tool.png "") 

*   Press "Open Image Directory" to select folder containning images and annotation files which is generated from steps in b.
    
    ![alt text](img/select_work_dir.png "") 
    
    The tool will open the folder and load the first image in alphabet list.
    
    ![alt text](img/main_window.png " ")
    
*   Use buttons "Previous Image" or "Next Images" to browse images in directory, or select image directory from  listbox

*   Press "Delete Image" to Delete current displayed image, as we sure there is no acceptable face on image

*   Use Browse Boxes's buttons to select, add, delete or edit bounding boxes' person ids or locations

*   Frame "Persons and Annoated Images" shows list of annotated persons (via their ids) and corresponding annotated images


  
**2. Inference**

**3. Evaluation**
